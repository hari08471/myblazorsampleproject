﻿using EmployeeManagement.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement.Api.Models
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly AppDBContext appDBContext;
        public DepartmentRepository(AppDBContext appDBContext)
        {
            this.appDBContext = appDBContext;
        }
        public async Task<Department> GetDepartment(int DepartmentId)
        {
            return await appDBContext.Departments
                .FirstOrDefaultAsync(e => e.DepartmentId == DepartmentId);
        }

        public async Task<IEnumerable<Department>> GetDepartments()
        {
            return await appDBContext.Departments.ToListAsync();
        }
    }
}
