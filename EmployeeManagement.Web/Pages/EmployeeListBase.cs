﻿using EmployeeManagement.Models;
using EmployeeManagement.Web.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement.Web.Pages
{
    public class EmployeeListBase : ComponentBase
    {

        [Inject]
        public IEmployeeService employeeService { get; set; }
        public IEnumerable<Employee> Employees { get; set; }

        public bool ShowFooter { get; set; } = true;
        public IDepartmentService departmentService { get; set; }
        public List<Department> Departments { get; set; } //= new List<Department>();

        protected override async Task OnInitializedAsync()
        {
          Employees =  (await employeeService.GetEmployees()).ToList();
           // Departments = (await departmentService.GetDepartments()).ToList();
            // await Task.Run(LoadEmployees);

        }
        private void LoadEmployees()
        {
            System.Threading.Thread.Sleep(3000);
            Employee e1 = new Employee { 
             EmployeeId =1,
             FirstName ="Hari krishna",
             LastName = "Nagothu",
             Email= "hari.nagothu@gmail.com",
             DateOfBirth = new DateTime(1991,5 ,17),
             Gender = Gender.Male,
             DepartmentId  =1,
             PhotoPath ="Images/Hari.png"
            };

            Employee e2 = new Employee
            {
                EmployeeId = 2,
                FirstName = "Sai mallik",
                LastName = "Kondari",
                Email = "sai.kondari@gmail.com",
                DateOfBirth = new DateTime(1993, 12, 06),
                Gender = Gender.Male,
                DepartmentId = 2,
                PhotoPath = "Images/Sai.png"
            };

            Employee e3 = new Employee
            {
                EmployeeId = 3,
                FirstName = "Rajesh",
                LastName = "Maddipati",
                Email = "rajesh.maddipati@gmail.com",
                DateOfBirth = new DateTime(1991, 12, 06),
                Gender = Gender.Male,
                DepartmentId = 3,
                PhotoPath = "Images/Rajesh.png"
            };

            Employee e4 = new Employee
            {
                EmployeeId = 4,
                FirstName = "Satya narayana",
                LastName = "Vanga",
                Email = "satyacse94@gmail.com",
                DateOfBirth = new DateTime(1983, 12, 06),
                Gender = Gender.Male,
                DepartmentId = 4,
                PhotoPath = "Images/Satya.png"
            };

            Employees = new List<Employee> { e1, e2, e3, e4 };
        }
    }
}
