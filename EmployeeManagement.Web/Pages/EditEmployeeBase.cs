﻿using EmployeeManagement.Models;
using EmployeeManagement.Web.Models;
using EmployeeManagement.Web.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace EmployeeManagement.Web.Pages
{
    public class EditEmployeeBase : ComponentBase
    {
        [CascadingParameter]
        public Task<AuthenticationState> authenticationStateTask { get; set; }

        [Inject]
        public IEmployeeService employeeService { get; set; }
        public Employee Employee { get; set; } = new Employee();

        public EditEmployeeModel EditEmployeeModel { get; set; } = new EditEmployeeModel();

        public IDepartmentService departmentService { get; set; }

        public List<Department> Departments { get; set; } = new List<Department>();

        public string PageHeaderText { get; set; }

        [Parameter]
        public string Id { get; set; }

        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var authenticationState = await authenticationStateTask;

            if (!authenticationState.User.Identity.IsAuthenticated)
            {
                string returnUrl = WebUtility.UrlEncode($"editEmployee/{Id}");
                NavigationManager.NavigateTo($"/identity/account/login?returnUrl={returnUrl}");
            }

            int.TryParse(Id, out int EmployeeId);

            if(EmployeeId != 0)
            {
                PageHeaderText = "Edit Employee";
                Employee = (await employeeService.GetEmployee(int.Parse(Id)));
            }
            else
            {
                PageHeaderText = "Create Employee";
                Employee = new Employee
                {
                    DepartmentId = 1,
                    DateOfBirth = DateTime.Now,
                    PhotoPath = "nophoto.png",
                    Department = new Department { DepartmentName = "One"}
                };
            }


           
            // Departments = ((List<Department>)await departmentService.GetDepartments());
            LoadDepartments();

            EditEmployeeModel.EmployeeId = Employee.EmployeeId;
            EditEmployeeModel.FirstName = Employee.FirstName;
            EditEmployeeModel.LastName = Employee.LastName;
            EditEmployeeModel.DateOfBirth = Employee.DateOfBirth;
            EditEmployeeModel.DepartmentId = Employee.DepartmentId;
            EditEmployeeModel.Email = Employee.Email;
            EditEmployeeModel.Gender = Employee.Gender;
            EditEmployeeModel.PhotoPath = Employee.PhotoPath;
            EditEmployeeModel.ConfirmEmail = Employee.Email;
            EditEmployeeModel.Department = Employee.Department;
        }

        private void LoadDepartments()
        {

            Department e1 = new Department { DepartmentName = "Admin", DepartmentId = 1 };
            Department e2 = new Department { DepartmentId = 2, DepartmentName = "HR" };
            Departments.Add(e1);
            Departments.Add(e2);
        }

        protected async Task HandleValidSubmit()
        {
            Employee result = null;

            Employee.EmployeeId = EditEmployeeModel.EmployeeId;
            Employee.FirstName = EditEmployeeModel.FirstName;
            Employee.LastName = EditEmployeeModel.LastName;
            Employee.DateOfBirth = EditEmployeeModel.DateOfBirth;
            Employee.DepartmentId = EditEmployeeModel.DepartmentId;
            Employee.Email = EditEmployeeModel.Email;
            Employee.Gender = EditEmployeeModel.Gender;
            Employee.PhotoPath = EditEmployeeModel.PhotoPath;
            Employee.Department = EditEmployeeModel.Department;

            if(Employee.EmployeeId != 0)
            {
                result = await employeeService.UpdateEmployee(Employee);
            }
            else
            {
                result = await employeeService.CreateEmployee(Employee);
            }

           

            if(result != null)
            {
                NavigationManager.NavigateTo("/");
            }
        }

        protected async Task Delete_Click()
        {
            await employeeService.DeleteEmployee(Employee.EmployeeId);

            NavigationManager.NavigateTo("/");
        }


    }
}
