#pragma checksum "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\Pages\DataBindingDemo.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "76de57ee06a9f4c14b7b70b63c1e433f86ef2c26"
// <auto-generated/>
#pragma warning disable 1591
namespace EmployeeManagement.Web.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using EmployeeManagement.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using EmployeeManagement.Web.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\_Imports.razor"
using EmployeeManagement.Models;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/dataBindingDemo")]
    public partial class DataBindingDemo : DataBindingDemoBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<h3>DataBindingDemo</h3>\r\n\r\n\r\n");
            __builder.AddMarkupContent(1, "<h5>One way data binding</h5>\r\n\r\n");
            __builder.OpenElement(2, "div");
            __builder.AddMarkupContent(3, "<b>Name:</b> ");
            __builder.AddContent(4, 
#nullable restore
#line 10 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\Pages\DataBindingDemo.razor"
                  Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(5, "\r\n");
            __builder.OpenElement(6, "div");
            __builder.AddMarkupContent(7, "<b>Name with saluation: </b> ");
            __builder.AddContent(8, 
#nullable restore
#line 13 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\Pages\DataBindingDemo.razor"
                                   Gender == "Male" ? $"Mr. {Name}" : $"Miss. {Name}"

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(9, "\r\n");
            __builder.OpenElement(10, "div");
            __builder.AddMarkupContent(11, "<b>Name:</b> ");
            __builder.OpenElement(12, "input");
            __builder.AddAttribute(13, "value", 
#nullable restore
#line 16 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\Pages\DataBindingDemo.razor"
                                Name

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(14, "\r\n\r\n");
            __builder.AddMarkupContent(15, "<b>Description</b>\r\n");
            __builder.OpenElement(16, "div");
            __builder.OpenElement(17, "textarea");
            __builder.AddAttribute(18, "style", "width:500px; height:50px;");
            __builder.AddAttribute(19, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 21 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\Pages\DataBindingDemo.razor"
                     Description

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(20, "oninput", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => Description = __value, Description));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(21, "\r\n");
            __builder.OpenElement(22, "div");
            __builder.AddMarkupContent(23, "\r\n    Count: ");
            __builder.AddContent(24, 
#nullable restore
#line 24 "C:\Users\52253942\source\repos\BlazerTutorial\EmployeeManagement.Web\Pages\DataBindingDemo.razor"
            Description.Length

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
